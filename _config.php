<?php

user_error(
	'The CWP fork of silverstripe/advancedworkflow has been merged into upstream. ' .
	'Please remove the module url from the "repositories" section of your "composer.json" ' .
	'to switch to upstream.',
	E_USER_ERROR
);
